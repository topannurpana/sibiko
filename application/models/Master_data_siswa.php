<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_data_siswa extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  function get_all(){
    return $this->db->get('v_siswa');
  }

  function get_walikelas(){
    return $this->db->get('t_walikelas');
  }

  function tambah_data($data,$table){

    $this->db->insert($table,$data);
  }

  function hapus_data($where,$table){
  $this->db->where($where);
  $this->db->delete($table);
  }

  function edit_data($where,$table){
	return $this->db->get_where($table,$where);
   }

   function update_data($where,$data,$table){
   $this->db->where($where);
   $this->db->update($table,$data);
 }

}
