<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="<?php echo base_url('assets/img/avatar.png') ?>" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $this->session->userdata('pengguna')->username;?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="<?php echo base_url('Dashboard'); ?>"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-datamaster">
				<em class="fa fa-navicon">&nbsp;</em> Data Master <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-datamaster">
					<li><a class="" href="<?php echo base_url('Kelola_data_siswa') ?>">
						<span class="fa fa-arrow-right">&nbsp;</span> Kelola Data Siswa
					</a></li>
					<li><a class="" href="<?php echo base_url('Kelola_data_walikelas') ?>">
						<span class="fa fa-arrow-right">&nbsp;</span> Kelola Data Wali Kelas
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Kelola Score Pelanggaran
					</a></li>
          <li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Kelola Data Prestasi
					</a></li>
				</ul>
			</li>

      <li class="parent "><a data-toggle="collapse" href="#sub-laporan">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-laporan">
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Laporan Pelanggaran
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Laporan Prestasi
					</a></li>
				</ul>
			</li>
			<li><a href="<?php echo base_url('Login/log_out') ?>"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
