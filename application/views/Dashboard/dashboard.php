<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lumino - Dashboard</title>
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/datepicker3.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">

	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
  <header class="main-header">
   <?php $this->load->view('Dashboard/header'); ?>
  </header>

    <aside class="main-sidebar">

       <!-- sidebar: style can be found in sidebar.less -->
       <section class="sidebar">
       <?php $this->load->view('Dashboard/sidebar'); ?>
           <!-- /.sidebar-menu -->
       </section>
       <!-- /.sidebar -->
     </aside>
     <div class="row">
       <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
         <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
              <ol class="breadcrumb">
          				<li><a href="#">
          					<em class="fa fa-home"></em>
          				</a></li>
          				<li class="active"><?php echo $judul ?></li>
          			</ol>
          		</div><!--/.row-->

          		<div class="row">
          			<div class="col-lg-12">
          				<h1 class="page-header"><?php echo $judul ?></h1>
          			</div>
    		     </div><!--/.row-->
          </section>

          <!-- Main content -->
          <section class="content container-fluid">

            <?php $this->load->view($content); ?>

          </section>
          <!-- /.content -->
        </div>
       </div>
     </div>

  <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/chart.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/chart-data.js"></script>
	<script src="<?php echo base_url();?>assets/js/easypiechart.js"></script>
	<script src="<?php echo base_url();?>assets/js/easypiechart-data.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url();?>assets/js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

</body>
</html>
