<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Form Tambah Data Siswa</div>
					<div class="panel-body">
            <form action="<?php echo base_url(). '/kelola_data_siswa/update'; ?>" method="post">
              <input type="hidden" name="id" value="<?php echo $r->id ?>">
              <div class="container">
                 <div class="row">
                   <div class="col-sm-5">
                     <label for="id_waikelas">id walikelas</label>
										 <select name="id_waikelas" class="form-control">
                                      <option value="<?php echo $r->id_waikelas; ?>"><?php echo $r->id_waikelas; ?></option>
                                      <?php $row = $this->Master_data_siswa->get_walikelas()->result_array();
                                      foreach ($row as $key => $value): ?>
                                      <option value="<?php echo $value['id'];?>"><?php echo $value['id'];?>|<?php echo $value['nign']; ?>|<?php echo $value['nama_walikelas']; ?></option>
                                      <?php endforeach ?>
                     </select>

                     <label for="nisn_siswa">Nisn</label>
                     <input type="text" class="form-control" name="nisn_siswa" value="<?php echo $r->nisn_siswa; ?>" />

                     <label for="nama_siswa">Nama_siswa</label>
                     <input type="text" class="form-control" name="nama_siswa" value="<?php echo $r->nama_siswa; ?>" />

										 <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tanggal_lahir" value="<?php echo $r->tanggal_lahir; ?>" />
                  </div>
                  <div class="col-sm-5">
                      <br>
										<label for="jenis_kelamink">Jenis Kelamin</label>
											<select class="form-control" name="jenis_kelamin">
												<option value="<?php echo $r->jenis_kelamin; ?>"><?php echo $r->jenis_kelamin; ?></option>
												<option value="Laki - Laki">Laki - Laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
										<label for="jurusan">Jurusan</label>
												<select class="form-control" name="jurusan">
													<option value="<?php echo $r->jurusan ?>"><?php echo $r->jurusan ?></option>
													<option value="Tata Busana">Tata Busana</option>
													<option value="Pemasaran">Pemasaran</option>
												</select>
										<label for="tahun_masuk_sekolah">Tahun Masuk Sekolah</label>
										<input class="form-control" type="date" name="tahun_masuk_sekolah" value="<?php echo $r->tahun_masuk_sekolah; ?>">

										<br>
                    <input class="btn btn-primary" type="submit" name="kirim" value="Update">
										<a class="btn btn-danger" href="<?php echo base_url('Kelola_data_siswa') ?>">Cancel</a>
                  </div>
                 </div>
              </div>
            </body>

            <?php form_close(); ?>
					</div>
				</div>
			</div>
