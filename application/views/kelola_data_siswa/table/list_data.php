<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Default Panel</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8">
								<a class="btn btn-primary" href="<?php echo base_url('Kelola_data_siswa/tambah_data') ?>">Tambah Data</a>
							</div>
							<div class="col-md-2">
								<input class="form-control" type="text" name="" value="">
							</div>
							<div class="col-md-2">
								<a class="btn btn-default btn-lg" href="#"><span></span><i class="glyphicon glyphicon-search"></i></a>
							</div>
						</div>
            <table class="table table-hovered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Siswa</th>
                  <th>Nama Wali Kelas</th>
                  <th>Jurusan</th>
                  <th>Gender</th>
                  <th>Tanggal lahir</th>
                  <th>Tahun Masuk</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                foreach ($row as $r): ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $r->nama_siswa;?></td>
                    <td><?php echo $r->nama_walikelas;?></td>
                    <td><?php echo $r->jurusan;?></td>
                    <td><?php echo $r->jksiswa;?></td>
                    <td><?php echo $r->tanggal_lahir;?></td>
                    <td><?php echo $r->tahun_masuk_sekolah;?></td>
                    <td>
                      <a class="btn btn-warning" href="<?php echo base_url('index.php/Kelola_data_siswa/edit/').$r->idsiswa ;?>"><i class="glyphicon glyphicon-pencil"></i></a>
                      <a class="btn btn-danger" href="<?php echo base_url('index.php/Kelola_data_siswa/hapus/').$r->idsiswa ;?>"><i class="glyphicon glyphicon-trash"></i></a>

                    </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
					</div>
				</div>
			</div>
