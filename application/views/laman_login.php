<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lumino - Login</title>
	<link href="<?php echo base_url() ;?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ;?>assets/css/datepicker3.css" rel="stylesheet">
	<link href="<?php echo base_url() ;?>assets/css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-success">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" action="<?php echo base_url()?>Login/do_login/" method="post" />
            <?php
                    if (validation_errors() || $this->session->flashdata('result_login')) {
                        ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Warning!</strong>
                            <?php echo validation_errors(); ?>
                            <?php echo $this->session->flashdata('result_login'); ?>
                        </div>
                    <?php } ?>
                  <div class="input-group">
        					  <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
        					    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
        					</div>
        					 <br>
        					 <div class="input-group">
        					   <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
        					    <input type="password" class="form-control" name="password" id="password" placeholder="password">
        					 </div>
                   <br>
        					   <div class="text-center">
        					 	   <input type="submit" class="btn btn-success" name="login" value="Login">
        					 </div>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->


<script src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
