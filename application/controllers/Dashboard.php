<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $data['judul'] = 'Dashboard';
    $data['content'] = 'v_dashboard';
		$this->load->view('Dashboard/dashboard', $data);
  }

}
