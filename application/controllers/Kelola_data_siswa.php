<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelola_data_siswa extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
      $this->load->model('Master_data_siswa');
  }

  function index()
  {
    $data['judul'] = 'List Data Siswa';
    $data['content'] = 'kelola_data_siswa/table/list_data';
    $data['row'] = $this->Master_data_siswa->get_all()->result();
		$this->load->view('Dashboard/dashboard', $data);
  }

  function tambah_data(){
    $data['judul'] = 'Tambah Data Siswa';
    $data['content'] = 'kelola_data_siswa/form/tambah_data';
		$this->load->view('Dashboard/dashboard', $data);
  }

  function tambah_aksi(){
    $id_waikelas = $this->input->post('id_waikelas');
    $nisn_siswa = $this->input->post('nisn_siswa');
    $nama_siswa = $this->input->post('nama_siswa');
    $tanggal_lahir = $this->input->post('tanggal_lahir');
    $jenis_kelamin = $this->input->post('jenis_kelamin');
    $jurusan = $this->input->post('jurusan');
    $tahun_masuk_sekolah = $this->input->post('tahun_masuk_sekolah');

    $data = array(
      'id_waikelas' => $id_waikelas,
      'nisn_siswa' => $nisn_siswa,
      'nama_siswa' => $nama_siswa,
      'tanggal_lahir' => $tanggal_lahir,
      'jenis_kelamin' => $jenis_kelamin,
      'jurusan' => $jurusan,
      'tahun_masuk_sekolah' => $tahun_masuk_sekolah
    );
    $this->Master_data_siswa->tambah_data($data,'t_siswa');
    redirect('kelola_data_siswa/index');
  }

  function edit($id){
    $where = array('id' => $id);
    $data['wlk'] = $this->Master_data_siswa->get_walikelas()->row();
    $data['r'] = $this->Master_data_siswa->edit_data($where,'t_siswa')->row();
    $data['judul'] = 'Edit Data Siswa';
    $data['content'] = 'kelola_data_siswa/form/edit_data';
    $this->load->view('Dashboard/dashboard',$data);
  }

  function update(){
    $id = $this->input->post('id');
    $id_waikelas = $this->input->post('id_waikelas');
    $nisn_siswa = $this->input->post('nisn_siswa');
    $nama_siswa = $this->input->post('nama_siswa');
    $tanggal_lahir = $this->input->post('tanggal_lahir');
    $jenis_kelamin = $this->input->post('jenis_kelamin');
    $jurusan = $this->input->post('jurusan');
    $tahun_masuk_sekolah = $this->input->post('tahun_masuk_sekolah');

	$data = array(
    'id_waikelas' => $id_waikelas,
    'nisn_siswa' => $nisn_siswa,
    'nama_siswa' => $nama_siswa,
    'tanggal_lahir' => $tanggal_lahir,
    'jenis_kelamin' => $jenis_kelamin,
    'jurusan' => $jurusan,
    'tahun_masuk_sekolah' => $tahun_masuk_sekolah
	);

	$where = array(
		'id' => $id
	);

	$this->Master_data_siswa->update_data($where,$data,'t_siswa');
	redirect('Kelola_data_siswa/index');
}

function hapus($id){
    $where = array('id' => $id);
    $this->Master_data_siswa->hapus_data($where,'t_siswa');
    redirect('kelola_data_siswa/index');
  }

}
