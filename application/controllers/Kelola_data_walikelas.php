<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelola_data_walikelas extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $data['judul'] = 'List Data Walikelas';
    $data['content'] = 'kelola_data_walikelas/table/list_data';
    $this->load->view('Dashboard/dashboard', $data);
  }

}
